variable storage_account_name {
  type    = string
  default = "saltoksstorage"
}

variable resource_group_name {
  type    = string
  default = "saltoksresourcegroup"
}

variable resource_group_location {
  type    = string
  default = "westeurope"
}

variable storage_account_tier {
  type    = string
  default = "Standard"
}

variable storage_account_replication_type {
  type    = string
  default = "GRS"
}