variable resource_group_name {
  type    = string
  default = "saltoks-devops-test"
}

variable resource_group_location {
  type    = string
  default = "westeurope"
}
