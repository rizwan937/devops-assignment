terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }
}

provider "azurerm" {
  features {}
}

module "resource_group" {
  source = "../modules/ResourceGroup"

  resource_group_name  = "saltoks-devops-test-v4"
  resource_group_location  = "westeurope"
}


module "storage" {
  source = "../modules/Storage"

  storage_account_name = "saltoksstoragev7"
  resource_group_name = module.resource_group.resource_group_name
  resource_group_location = module.resource_group.resource_group_location
  storage_account_tier = "Standard"
  storage_account_replication_type = "GRS"
}