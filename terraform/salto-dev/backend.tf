terraform {
  backend "azurerm" {
    resource_group_name  = "saltoks-devops-test"
    storage_account_name = "saltokstfstate"
    container_name       = "tfstate"
    key                  = "dev.terraform.tfstate"
  }
}
