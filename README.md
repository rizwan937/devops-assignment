# devops-assignment
In order to run the terraform. Please follow below mentioned commands:
1. Change in any module insides modules directory.
2. Please do not change backend without approval, since it can damage existing resources.
3. Please create your own branch starting with either feature/<feature_name> or hotfix/<hotfix_name>
4. Once you change module or environment, please raise PR to master branch since direct push to master is prohibited and you can only get to master using PR.


